import Foundation


struct Product {
    var name: String
    var price: Int
    var imageName: String
}

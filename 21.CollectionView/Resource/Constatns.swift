import Foundation
import UIKit

class Constants {
    
    enum CellNibName: String {
        case product = "ProductCollectionViewCell"
        case stock = "StockCollectionViewCell"
        case advertising = "AdvertisingCollectionViewCell"
    }
    
    enum CellId: String {
        case product = "productCell"
        case stock = "stockCell"
        case advertising = "advertisingCell"
    }
    
    struct DataForCollection {
        static let stockImages = [
            UIImage(named: "s1"),
            UIImage(named: "s2"),
            UIImage(named: "s3"),
            UIImage(named: "s4"),
            UIImage(named: "s5")
        ]
        
        static let products = [
            Product(name: "Бритва Гилет", price: 12, imageName: "p1"),
            Product(name: "SSD Samsung", price: 43, imageName: "p2"),
            Product(name: "Блендер", price: 144, imageName: "p3"),
            Product(name: "Фен", price: 10, imageName: "p4"),
            Product(name: "Арахисовая паста", price: 18, imageName: "p5"),
            Product(name: "Подушка", price: 564, imageName: "p6"),
            Product(name: "Nentendo switch", price: 243, imageName: "p7"),
            Product(name: "Samsund A71", price: 65, imageName: "p8"),
            Product(name: "Light mop", price: 18, imageName: "p9"),
            Product(name: "Штора", price: 132, imageName: "p10"),
            Product(name: "Macbook Pro", price: 231, imageName: "p11"),
            Product(name: "Huiwei P50", price: 67, imageName: "p12"),
            Product(name: "Штаны", price: 89, imageName: "p13")
        ]
        
        static let advertisingImages = [
            UIImage(named: "a1"),
            UIImage(named: "a2"),
            UIImage(named: "a3"),
            UIImage(named: "a4")
        ]
    }
}

import UIKit

class ViewController: UIViewController {

    var gradientView = UIView()
    var backgroundView = UIView()
    var searchBar = UISearchBar()
    var stockCollectionView: UICollectionView!
    var pageControl = UIPageControl()
    var hitLabel = UILabel()
    var productsCollectionView: UICollectionView!
    var advertisingCollectionView: UICollectionView!
    
    var isSearching: Bool = false {
        didSet {
            productsCollectionView.reloadData()
        }
    }
    var searchingProducts = [Product]()
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        setGradient(subView: gradientView)
        initSearchBar()
        initBackgroundView()
        setNewPost()
    }

    //MARK: Gradient
    private func setGradient(subView: UIView) {
        gradientView = UIView(frame: view.bounds)
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.colors = [
            #colorLiteral(red: 0.7959111333, green: 0.0605179742, blue: 0.6694757938, alpha: 1).cgColor,
            #colorLiteral(red: 0.2989752293, green: 0.0659827888, blue: 0.455904305, alpha: 1).cgColor
        ]
        gradient.startPoint = .init(x: 0, y: 0.5)
        gradient.endPoint = .init(x: 1, y: 0.5)
        gradientView.layer.addSublayer(gradient)
        view.addSubview(gradientView)
    }
    
    //MARK: SearchBar
    private func initSearchBar() {
        searchBar = UISearchBar(frame: .init(x: 0, y: 20, width: view.frame.width, height: 40))
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBarSetup()
        view.addSubview(searchBar)
    }

    private func searchBarSetup() {
        guard
            let searchBarTextField = searchBar.value(forKey: "searchField") as? UITextField
        else { return }
        searchBarTextField.backgroundColor = .white
        searchBarTextField.alpha = 0.3
    }
    
    //MARK: Background View
    private func initBackgroundView() {
        backgroundView = UIView(
            frame: .init(
                x: 0,
                y: searchBar.frame.maxY+10,
                width: view.frame.width,
                height: view.frame.height-searchBar.frame.height
            )
        )
        backgroundView.backgroundColor = .white
        backgroundViewSetup()
        view.addSubview(backgroundView)
    }
    
    private func setCorner() {
        let cornterPath = UIBezierPath(
            roundedRect: backgroundView.bounds,
            byRoundingCorners: [
                .topLeft,
                .topRight
            ],
            cornerRadii: CGSize(width: 15, height: 15)
        )
        let shareLayer = CAShapeLayer()
        shareLayer.path = cornterPath.cgPath
        
        backgroundView.layer.mask = shareLayer
    }
    
    //MARK: BackgroundView Setup
    private func backgroundViewSetup() {
        setCorner()
     
        initStockCollectionView()
        initPageControl()
        initLabel()
        initProductsCollectionView()
        initAdvertisingCollectionView()
    }
    
    //MARK: Stock CollectionView
    private func initStockCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = .init(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = .init(width: 0, height: 0)
        layout.scrollDirection = .horizontal
        stockCollectionView = UICollectionView(
            frame: .init(
                x: Int(backgroundView.bounds.origin.x+10),
                y: Int(backgroundView.bounds.origin.y+10),
                width: Int(backgroundView.bounds.width)-10,
                height: Int(backgroundView.bounds.height)/6
            ),
            collectionViewLayout: layout
        )
        stockCollectionView.center.x = backgroundView.center.x
        stockCollectionView.showsHorizontalScrollIndicator = false
        stockCollectionView.backgroundColor = .clear
        stockCollectionView.delegate = self
        stockCollectionView.dataSource = self
        
        stockCollectionView.register(
            UINib(
                nibName: Constants.CellNibName.stock.rawValue,
                bundle: nil
            ),
            forCellWithReuseIdentifier: Constants.CellId.stock.rawValue
        )
        backgroundView.addSubview(stockCollectionView)
    }
    
    //MARK: Page Control
    private func initPageControl() {
        pageControl = UIPageControl(
            frame: .init(
                x: 0,
                y: Int(stockCollectionView.frame.maxY)+5,
                width: 180,
                height: 10
            )
        )
        pageControl.center.x = backgroundView.center.x
        pageControl.numberOfPages = Constants.DataForCollection.stockImages.count
        pageControl.currentPage = 1
        pageControl.backgroundColor = .white
        
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.9897758365, green: 0.9319431186, blue: 0.9761613011, alpha: 1)
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.7959111333, green: 0.0605179742, blue: 0.6694757938, alpha: 1)
        backgroundView.addSubview(pageControl)
    }
    
    //MARK: Hit Label
    private func initLabel() {
        hitLabel = UILabel(
            frame: .init(
                x: stockCollectionView.frame.origin.x,
                y: pageControl.frame.maxY,
                width: 150,
                height: 30
            )
        )
        hitLabel.font = UIFont.boldSystemFont(ofSize: 15)
        hitLabel.text = "Хиты продаж"
        backgroundView.addSubview(hitLabel)
    }
    
    //MARK: Product CollectionView
    private func initProductsCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = .init(width: 0, height: 0) 
        productsCollectionView = UICollectionView(
            frame: .init(
                x: 0,
                y: hitLabel.frame.maxY,
                width: backgroundView.bounds.width,
                height: backgroundView.bounds.height/2+10
            ),
            collectionViewLayout: layout
        )
        productsCollectionView.backgroundColor = .clear
        productsCollectionView.register(
            UINib(
                nibName: Constants.CellNibName.product.rawValue,
                bundle: nil
            ),
            forCellWithReuseIdentifier: Constants.CellId.product.rawValue
        )
        productsCollectionView.delegate = self
        productsCollectionView.dataSource = self
        backgroundView.addSubview(productsCollectionView)
    }
    
    //MARK: Advertising CollectionView
    private func initAdvertisingCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = .init(width: 0, height: 0)
        layout.scrollDirection = .horizontal
        advertisingCollectionView = UICollectionView(
            frame: .init(
                x: 0,
                y: productsCollectionView.frame.maxY+5,
                width: backgroundView.bounds.width,
                height: backgroundView.frame.height/7
            ),
            collectionViewLayout: layout
        )
    
        advertisingCollectionView.showsHorizontalScrollIndicator = false
        advertisingCollectionView.register(
            UINib(
                nibName: Constants.CellNibName.advertising.rawValue,
                bundle: nil
            ),
            forCellWithReuseIdentifier: Constants.CellId.advertising.rawValue
        )
        advertisingCollectionView.delegate = self
        advertisingCollectionView.dataSource = self
        advertisingCollectionView.backgroundColor = .clear
        backgroundView.addSubview(advertisingCollectionView)
    }
    
    private func setNewPost() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(newPost),
            name: NSNotification.Name(rawValue: "Time"),
            object: nil
        )
    }
    
    //MARK: Handlers
    @objc
    private func newPost() {
        count+=1
        let indexPath = IndexPath(row: count, section: 0)
        stockCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        if count == Constants.DataForCollection.stockImages.count {
            count = 0
        }
    }
}

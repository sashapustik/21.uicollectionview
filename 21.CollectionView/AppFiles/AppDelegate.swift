import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    var timer = Timer()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
            NotificationCenter.default.post(
                Notification(
                    name: Notification.Name(rawValue: "Time"),
                    object: nil,
                    userInfo: ["value": true]
                )
            )
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

}


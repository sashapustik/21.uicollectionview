import Foundation
import UIKit

//MARK: DataSource
extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == stockCollectionView {
            pageControl.numberOfPages = Constants.DataForCollection.stockImages.count
            return Constants.DataForCollection.stockImages.count
        } else if collectionView == advertisingCollectionView {
            return Constants.DataForCollection.advertisingImages.count
        } else if collectionView == productsCollectionView {
            return isSearching
                ? searchingProducts.count
                : Constants.DataForCollection.products.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == stockCollectionView {
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Constants.CellId.stock.rawValue,
                    for: indexPath
                ) as? StockCollectionViewCell
            else { return UICollectionViewCell() }
            
            cell.stockImageView.image = Constants.DataForCollection.stockImages[indexPath.row]
            cell.layer.cornerRadius = 10
            return cell
        } else if collectionView == productsCollectionView {
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Constants.CellId.product.rawValue,
                    for: indexPath
                ) as? ProductCollectionViewCell
            else { return UICollectionViewCell() }
            
            cell.setCell(product: isSearching
                            ? searchingProducts[indexPath.row]
                            : Constants.DataForCollection.products[indexPath.row]
            )
            cell.backgroundColor = .white
            return cell
        } else if collectionView == advertisingCollectionView {
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Constants.CellId.advertising.rawValue,
                    for: indexPath
                ) as? AdvertisingCollectionViewCell
            else { return UICollectionViewCell() }
            
            cell.advertisingImageView.image = Constants.DataForCollection.advertisingImages[indexPath.row]
            cell.layer.cornerRadius = 10
            return cell
        }
        return UICollectionViewCell()
    }
}


//MARK: Delegate
extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == stockCollectionView {
            return .init(width: collectionView.frame.width-10, height: collectionView.frame.height)
        } else if collectionView == advertisingCollectionView {
            return .init(width: collectionView.frame.width/2-10, height: collectionView.frame.height)
        } else if collectionView == productsCollectionView {
            let value = view.bounds.width<400 ? 4 : 5
            return .init(width: collectionView.frame.width/CGFloat(value), height: collectionView.frame.height/2-10)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if collectionView == stockCollectionView {
            pageControl.currentPage = indexPath.row-1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == advertisingCollectionView {
            return .init(top: 0, left: 5, bottom: 0, right: 5)
        } else if collectionView == stockCollectionView {
            return .init(top: 0, left: 5, bottom: 0, right: 5)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == stockCollectionView {
            pageControl.currentPage = indexPath.row+1
            print(indexPath.row+1)
        }
    }
   
}

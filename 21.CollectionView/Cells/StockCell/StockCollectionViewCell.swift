import UIKit

class StockCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var stockImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        stockImageView.image = nil
    }
    

}

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCell(product: Product) {
        productImageView.image = UIImage(named: product.imageName)
        nameLabel.text = product.name
        priceLabel.text = "\(product.price) p. 00 к."
    }

}
